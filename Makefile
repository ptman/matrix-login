.PHONY: help
help:
	@cat Makefile

.PHONY: lint
lint:
	CGO_ENABLED=0 golangci-lint run .

.PHONY: build
build: matrix-login

matrix-login:
	CGO_ENABLED=0 GOOS=$$GOOS GOARCH=$$GOARCH go build -trimpath -ldflags "-s -w"

.PHONY: docker
docker:
	docker build -t matrix-login .

.PHONY: dockerx
dockerx:
	DOCKER_BUILDKIT=1 docker buildx build --platform linux/arm64,linux/amd64 -t registry.gitlab.com/ptman/matrix-login --push .

.PHONY: run
run:
	go run main.go

.PHONY: clean
clean:
	rm -f matrix-login
