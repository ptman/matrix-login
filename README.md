# Login using Matrix

Login using magic link over [Matrix](https://matrix.org) instead of email.
Similar to https://loginwithmatrix.tiktalk.space/ . More secure than handing
your password to services like https://news.ycombinator.com/item?id=29240154

## How it works?

1. Enter your Matrix ID
2. You will receive a message with a link
3. Follow the link in the same browser session that you used to enter your
   Matrix ID
4. You are logged in

## Pros

https://loginwithmatrix.tiktalk.space/ listed pros quite well:

- more secure than email or sms (matrix connections are TLS-encrypted, and e2ee
  would make it even better (but bridges can break this promise of
  confidentiality))
- decentralised
- can pull profile data from matrix
- can be used for notifications
- open source
- easy to use, just pick up your matrix client

## TODO

This is just a demo with source.

- e2ee
- better room permissions
- code instead of magic link?
- proper session store
- usable for others to build upon
- check security
- write tests
- ...

## Demo

https://matrix-login.lyc.fi

## Contact

I'm [@ptman:ptman.name](https://matrix.to/#/@ptman:ptman.name)

## License

ISC
