# syntax=docker/dockerfile:1
# vim: set ft=dockerfile:
FROM --platform=$BUILDPLATFORM golang:1.20-alpine3.17 AS buildgo
ARG TARGETOS TARGETARCH
RUN apk add --no-cache ca-certificates make
COPY . ${GOPATH}/matrix-login
WORKDIR ${GOPATH}/matrix-login
RUN go get -v
RUN env GOOS=${TARGETOS} GOARCH=${TARGETARCH} make matrix-login

FROM --platform=$TARGETPLATFORM scratch
WORKDIR /
COPY --from=buildgo /etc/ssl/ /etc/ssl/
COPY --from=buildgo /go/matrix-login/views/ /views/
COPY --from=buildgo /go/matrix-login/matrix-login /

EXPOSE 8080
CMD ["/matrix-login"]
