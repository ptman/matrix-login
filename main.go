package main

// like https://loginwithmatrix.tiktalk.space/

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gopkg.in/yaml.v3"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

const secureSize = 32 // 256bit
const sessionCookieName = "mlsession"

type session struct {
	ID          string
	Token       string
	TokenExpire time.Time
	Expire      time.Time
	User        string
	Auth        bool
}

type config struct {
	Homeserver   string `yaml:"homeserver"`
	Localpart    string `yaml:"localpart"`
	Password     string `yaml:"password"`
	AccessToken  string `yaml:"accessToken"`
	Listen       string `yaml:"listen"`
	BaseURL      string `yaml:"baseURL"`
	CookieSecure bool   `yaml:"cookieSecure"`
	userID       id.UserID
	hsURL        *url.URL
	baseURL      *url.URL
}

var conf *config

var client *mautrix.Client
var slock *sync.Mutex
var sessions map[string]session

func serveFile(name string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, name)
	}
}

func randomToken() string {
	buf := make([]byte, secureSize)
	if _, err := io.ReadFull(rand.Reader, buf); err != nil {
		panic(err)
	}

	return base64.RawURLEncoding.EncodeToString(buf)
}

func findOrCreateRoomFor(userID id.UserID) (id.RoomID, error) {
	roomID := id.RoomID("")

	rooms, err := client.JoinedRooms()
	if err != nil {
		return roomID, err
	}

	for i := range rooms.JoinedRooms {
		members, err := client.JoinedMembers(rooms.JoinedRooms[i])
		if err != nil {
			return roomID, err
		}

		if len(members.Joined) != 2 {
			continue
		}

		if _, ok := members.Joined[userID]; !ok {
			continue
		}

		roomID = rooms.JoinedRooms[i]
	}

	if roomID == "" {
		room, err := client.CreateRoom(&mautrix.ReqCreateRoom{
			Preset:   "trusted_private_chat",
			IsDirect: true,
			Topic:    fmt.Sprintf("messages from %s", conf.BaseURL),
		})
		if err != nil {
			return roomID, err
		}

		roomID = room.RoomID

		if _, err := client.InviteUser(roomID, &mautrix.ReqInviteUser{
			UserID: userID,
			Reason: fmt.Sprintf("authentication link for %s", conf.BaseURL),
		}); err != nil {
			return roomID, err
		}
	}

	return roomID, nil
}

func messageTo(userID id.UserID, msg string) error {
	roomID, err := findOrCreateRoomFor(userID)
	if err != nil {
		return err
	}

	_, err = client.SendNotice(roomID, msg)

	return err
}

func readConfig() error {
	f, err := os.Open("config.yaml")
	if err != nil {
		return err
	}

	conf = &config{}

	if err := yaml.NewDecoder(f).Decode(conf); err != nil {
		return err
	}

	conf.hsURL, err = url.Parse(conf.Homeserver)
	if err != nil {
		return err
	}

	conf.baseURL, err = url.Parse(conf.BaseURL)
	if err != nil {
		return err
	}

	conf.userID = id.NewUserID(conf.Localpart, conf.hsURL.Host)

	slock = &sync.Mutex{}
	sessions = make(map[string]session)

	return nil
}

func postLogin(w http.ResponseWriter, r *http.Request) {
	mxid := r.FormValue("mxid")
	sess := session{
		ID:          randomToken(),
		Expire:      time.Now().Add(time.Hour),
		User:        mxid,
		Token:       randomToken(),
		TokenExpire: time.Now().Add(10 * time.Minute),
	}

	magiclink, err := url.Parse(fmt.Sprintf("/login/%s", sess.Token))
	if err != nil {
		log.Printf("%+v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if err := messageTo(id.UserID(mxid),
		fmt.Sprintf("Please visit %s using the original session before %s",
			conf.baseURL.ResolveReference(magiclink),
			sess.TokenExpire.Format(time.RFC3339),
		)); err != nil {
		log.Printf("%+v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	cookie := &http.Cookie{
		Name:     sessionCookieName,
		Value:    sess.ID,
		SameSite: http.SameSiteLaxMode,
		Secure:   conf.CookieSecure,
		HttpOnly: true,
		Path:     "/",
		Expires:  sess.Expire,
		MaxAge:   int(time.Until(sess.Expire).Seconds()),
	}

	http.SetCookie(w, cookie)

	slock.Lock()
	sessions[sess.ID] = sess
	defer slock.Unlock()

	fmt.Fprintf(w, "check your messages and click on the link from %s", conf.userID)
}

func getLogin(w http.ResponseWriter, r *http.Request) {
	token := chi.URLParam(r, "token")
	cookie, err := r.Cookie(sessionCookieName)
	if errors.Is(err, http.ErrNoCookie) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "cookie missing, did you open link in correct browser session?")

		return
	} else if err != nil {
		log.Printf("%+v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	slock.Lock()
	defer slock.Unlock()
	sess, ok := sessions[cookie.Value]
	if !ok || sess.Expire.Before(time.Now()) {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if token != sess.Token || sess.TokenExpire.Before(time.Now()) {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	sess.Auth = true
	sess.Token = ""
	sess.TokenExpire = time.Unix(0, 0)
	sessions[sess.ID] = sess

	fmt.Fprintf(w, "welcome %s", sess.User)
}

func main() {
	err := readConfig()
	if err != nil {
		panic(err)
	}

	client, err = mautrix.NewClient(conf.Homeserver, conf.userID,
		conf.AccessToken)
	if err != nil {
		panic(err)
	}

	if client.AccessToken == "" {
		_, err = client.Login(&mautrix.ReqLogin{
			Type: "m.login.password",
			Identifier: mautrix.UserIdentifier{
				Type: mautrix.IdentifierTypeUser,
				User: conf.Localpart,
			},
			Password:         conf.Password,
			StoreCredentials: true,
		})
		if err != nil {
			panic(err)
		}

		log.Println("accesstoken", client.AccessToken)
	}

	r := chi.NewMux()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", serveFile("views/index.html"))
	r.Post("/login", postLogin)
	r.Get("/login/{token}", getLogin)

	log.Println("Listening on", conf.Listen)
	log.Fatal(http.ListenAndServe(conf.Listen, r))
}
